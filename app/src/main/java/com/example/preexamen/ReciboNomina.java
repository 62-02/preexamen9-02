package com.example.preexamen;

public class ReciboNomina {
    private int numRecibo;
    private String nombre;
    private int horasNormales;
    private int horasExtras;
    private int puesto;
    private double porcentajeImpuesto;

    private final double pagoBase = 200.0;

    public ReciboNomina(int numRecibo, String nombre, int horasNormales, int horasExtras, int puesto, double porcentajeImpuesto) {
        this.numRecibo = numRecibo;
        this.nombre = nombre;
        this.horasNormales = horasNormales;
        this.horasExtras = horasExtras;
        this.puesto = puesto;
        this.porcentajeImpuesto = porcentajeImpuesto;
    }

    private double obtenerPagoPorHora() {
        switch (puesto) {
            case 1:
                return pagoBase * 1.2;
            case 2:
                return pagoBase * 1.5;
            case 3:
                return pagoBase * 2.0;
            default:
                return pagoBase;
        }
    }

    public double calcularSubtotal() {
        double pagoPorHora = obtenerPagoPorHora();
        return (horasNormales * pagoPorHora) + (horasExtras * pagoPorHora * 2);
    }

    public double calcularImpuesto() {
        double subtotal = calcularSubtotal();
        return subtotal * (porcentajeImpuesto / 100);
    }

    public double calcularTotalAPagar() {
        double subtotal = calcularSubtotal();
        double impuesto = calcularImpuesto();
        return subtotal - impuesto;
    }
}
